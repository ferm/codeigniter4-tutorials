/**
 * Tutorial created by linuxitos <contact@linuxitos.com>
 */
-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 20, 2020 at 02:40 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_ajax_pagination`
--
DROP DATABASE IF EXISTS `ci_ajax_pagination`;
CREATE DATABASE IF NOT EXISTS `ci_ajax_pagination` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `ci_ajax_pagination`;

-- --------------------------------------------------------

--
-- Table structure for table `ajx_posts`
--

DROP TABLE IF EXISTS `ajx_posts`;
CREATE TABLE IF NOT EXISTS `ajx_posts` (
	`id_post` int(11) NOT NULL AUTO_INCREMENT,
	`fc_post` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
	`act_post` tinyint(4) NOT NULL DEFAULT 1,
	`nom_post` varchar(512) COLLATE utf8_spanish_ci DEFAULT NULL,
	`desc_post` varchar(512) COLLATE utf8_spanish_ci DEFAULT NULL,
	`slug_post` varchar(256) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'default',
	PRIMARY KEY (`id_post`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `ajx_posts`
--

INSERT INTO `ajx_posts` (`id_post`, `fc_post`, `act_post`, `nom_post`, `desc_post`, `slug_post`) VALUES
(1, '2020-04-17 20:01:35', 0, 'Activar tecla retroceso para regresar a la carpeta anterior en nautilus', '', 'activar-tecla-retroceso-para-regresar'),
(2, '2020-04-17 20:01:35', 0, 'Extensión de gnome-shell que no te deben faltar', '', 'extension-de-gnome-shell-que-no'),
(3, '2020-04-17 20:01:35', 0, 'Cambiando imagen del login de fedora', '', 'cambiando-imagen-del-login-de'),
(4, '2020-04-17 20:01:35', 0, 'Cambiando íconos en fedora 20', '', 'cambiando-iconos-en-fedora-20'),
(5, '2020-04-17 20:01:35', 0, 'Optimizar el peso de archivos pdf', '', 'optimizar-el-peso-de-archivos'),
(6, '2020-04-17 20:01:35', 0, 'Una consola elegante...', '', 'una-consola-elegante'),
(7, '2020-04-17 20:01:35', 0, 'Instalar zshell en fedora', '', 'instalar-zshell-en-fedora'),
(8, '2020-04-17 20:01:35', 0, 'Hacer que una maquina virtual detecte USB fedora 20', '', 'hacer-que-una-maquina-virtual'),
(9, '2020-04-17 20:01:35', 0, 'Activar/desactivar SeLinux en fedora', '', 'activar-desactivar-selinux-en-fedora'),
(10, '2020-04-17 20:01:35', 0, 'Recuperar grub en elementary OS luna junto con windows 8', '', 'recuperar-grub-en-elementary-os'),
(11, '2020-04-17 20:01:35', 0, 'Cómo cambiar el idioma de las páginas MAN', '', 'como-cambiar-el-idioma-de'),
(20, '2020-04-17 20:01:35', 1, 'Qué hacer después de instalar fedora 20 \"Heisenbug\" con escritorio gnome?', '', 'que-hacer-despues-de-instalar'),
(21, '2020-04-17 20:01:35', 1, 'Cambiar el motor de línea de comándos en fedora - linux', '', 'cambiar-el-motor-de-linea'),
(22, '2020-04-17 20:01:35', 1, 'Recuperar archivos de usb o hdd con foremost', '', 'recuperar-archivos-de-usb-o'),
(23, '2020-04-17 20:01:35', 1, 'Instalar MySQL Server y MysqlWorkbech en fedora 20 usando paquetes descargados', '', 'instalar-mysql-server-y-mysqlworkbech'),
(24, '2020-04-17 20:01:35', 1, 'Cómo configurar gt-recordMyDesktop para grabar tu escritorio sin problemas Gtk', '', 'como-configurar-gt-recordmydesktop-para-grabar'),
(25, '2020-04-17 20:01:35', 1, 'Cambiar la dirección MAC a la tarjeta de red de tu laptop con macchanger', '', 'cambiar-la-direccion-mac-a'),
(26, '2020-04-17 20:01:35', 1, 'Punteros en programas arduino', '', 'punteros-en-programas-arduino'),
(27, '2020-04-17 20:01:35', 1, 'Instalación de processing en fedora', '', 'instalacion-de-processing-en-fedora'),
(28, '2020-04-17 20:01:35', 1, 'Agregar un JCalendar a la paleta de componentes de netbeans', '', 'agregar-un-jcalendar-a-la'),
(29, '2020-04-17 20:01:35', 1, 'Obtener fecha del sistema y colocarlo en un JDateChooser Java', '', 'obtener-fecha-del-sistema-y'),
(30, '2020-04-17 20:01:35', 1, 'Script que avisa el estado de la carga de la batería de la laptop linux', '', 'script-que-avisa-el-estado'),
(31, '2020-04-17 20:01:35', 1, 'Crear instalador de aplicación programada en Visual Basic', '', 'crear-instalador-de-aplicacion-programada'),
(32, '2020-04-17 20:01:35', 1, 'Reproducir sonido en segundo plano usando hilos en java', '', 'reproducir-sonido-en-segundo-plano'),
(33, '2020-04-17 20:01:35', 1, 'Altas, Bajas, Consultas y Modificaciones desde aplicación java usando BD en mysql', '', 'altas-bajas-consultas-y-modificaciones'),
(34, '2020-04-17 20:01:35', 1, 'Instalar PostgreSQL y pgAdmin III en fedora', '', 'instalar-postgresql-y-pgadmin-iii'),
(35, '2020-04-17 20:01:35', 1, 'Matrices dinámicas en C', '', 'matrices-dinamicas-en-c'),
(36, '2020-04-17 20:01:35', 1, 'Reproducir sonidos en java, usando player y de forma más simples.', '', 'reproducir-sonidos-en-java-usando'),
(37, '2020-04-17 20:01:35', 1, 'Descargar una página web con wget', '', 'descargar-una-pagina-web-con'),
(38, '2020-04-17 20:01:35', 1, 'Instalación y uso de ClamAV', '', 'instalacion-y-uso-de-clamav'),
(39, '2020-04-17 20:01:35', 1, 'Kingsoft Office una alternativa a Microsoft Office', '', 'kingsoft-office-una-alternativa-a'),
(40, '2020-04-17 20:01:35', 1, 'Cronómetro en java', '', 'cronometro-en-java'),
(41, '2020-04-17 20:01:35', 1, 'Crear alias para facilitar tareas en comando de linux', '', 'crear-alias-para-facilitar-tareas'),
(42, '2020-04-17 20:01:35', 1, 'Instalación guestadition de VirtualBox en Kali Linux', '', 'instalacion-guestadition-de-virtualbox-en'),
(43, '2020-04-17 20:01:35', 1, 'Instalar ZSH en kali Linux', '', 'instalar-zsh-en-kali-linux'),
(44, '2020-04-17 20:01:35', 1, 'Conocer información de nuestro sistema', '', 'conocer-informacion-de-nuestro-sistema'),
(45, '2020-04-17 20:01:35', 1, 'Instalando temas de gnome shell', '', 'instalando-temas-de-gnome-shell'),
(46, '2020-04-17 20:01:35', 1, 'Instalar VirtualBox en fedora', '', 'instalar-virtualbox-en-fedora'),
(47, '2020-04-17 20:01:35', 1, 'Truco en Windows 8', '', 'truco-en-windows-8'),
(67, '2020-04-18 03:26:43', 1, 'Hola mundo', 'Adios', 'hola-mundo'),
(68, '2020-04-18 03:28:15', 1, 'Un sistema complejo desarrollado en react y angular', 'Un sistema complejo desarrollado en react y angular', 'un-sistema-complejo-desarrollado-en');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
	`id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
	`name` varchar(100) NOT NULL COMMENT 'Name',
	`type` varchar(255) NOT NULL COMMENT 'file type',
	`ext` varchar(30) NOT NULL DEFAULT 'png',
	`size` int(11) DEFAULT 0,
	`created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'Created date',
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='demo table';