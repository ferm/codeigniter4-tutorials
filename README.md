# Paginación en tablas, con ajax y codeigniter

![alt tag](./assets/app/images/img1.png)
![alt tag](./assets/app/images/img2.png)
![alt tag](./assets/app/images/img3.png)

## Descripción

Éstos ejemplos son de mi autoria, son una forma de solucionar los filtrados, busquedas de registros, eliminación, agregar, y sobre todo paginar los registros de tablas usando ajax.  En éste mismo ejemplo, se incluye también la parte de paginación de ajax completo para otras secciones. 

Hay varias opciones para paginar tablas, por ejemplo está jdatatables, sin embargo a veces tarda mucho en cargar, por lo tanto no hay nada mejor que hacer uno propio, pero esa es decisión de cada uno.

Características
================
- Boostrap 4.4.1
- Codeigniter 4.0.2
- Búsqueda en tablas directo a base de datos
- Filtro usando menú desplegable
- Subida de imágenes/archivos al servidor y base de datos
- Scritp de la base de datos

Uso
=====
	Lo primero a realizar es modificar la ruta de acceso al proyecto en el archivo app/config/App.php

Si al acceder a la ruta del proyecto desde un navegador, marcará error en la configuración de la base de datos.



#### Developed By
----------------
 * linuxitos - <contact@linuxitos.com>
